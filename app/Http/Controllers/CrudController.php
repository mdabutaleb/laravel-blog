<?php

namespace App\Http\Controllers;

use App\Crud;
use App\Http\Controllers\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;

class crudController extends Controller
{

    public function index()
    {
        $data = Crud::all();
        return view('crud.index')->with('Crud', $data);
    }

    public function create()
    {
        return view('crud.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $data = $request->all();
        Crud::create($data);


        return redirect()->back();

        $name = $request['title'];
        echo $name;

    }

}
