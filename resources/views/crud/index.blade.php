@extends('layout.master')

@section('content')




    <table class="table table-striped">
        <tr>
            <th>SL</th>
            <th>Title</th>
            <th>Body</th>
            <th>Action</th>
        </tr>
        @foreach ($Crud as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td><p align="justify"><b> {{ $item->title }}</b></p></td>
                <td><p align="justify"> {{ $item->content }}</p></td>
                <td>
                    <a href="{{ route('Crud.show', $item->id) }}">View</a> |
                    <a href="#">Edit</a> |
                    <a href="#">Delete</a>

                </td>
            </tr>
        @endforeach
    </table>

@endsection